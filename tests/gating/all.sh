#!/bin/bash

passed=0
subtests=(./compress ./decompress) 
total=${#subtests[@]}

for subtest in ${subtests[@]}
do
	pushd $subtest >/dev/null
	./test.sh
	result=$?
	echo "Test $subtest result: $result"
	if [ "$result" == "0" ]
       	then
		((passed++))
	fi
	popd >/dev/null
done

echo "Passed $passed/$total tests"
[[ $total == $passed ]] || exit 1
